#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void trieChaine( char* chaine)
{
    int taille = strlen(chaine);

    for(int i=0 ; i< taille ; i++)
    {
       for(int j=i+1 ; j<taille ; j++ )
       {
           if( chaine[i] > chaine[j] )
           {
               char tmp  = chaine[i];
               chaine[i] = chaine[j];
               chaine[j] = tmp;
           }
       }
    }

    printf("%s" , chaine);

}


int main( int argc, char * argv[])
{
    char* chaine = argv[1];
    trieChaine(chaine);

}
