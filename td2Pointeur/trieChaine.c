#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


char * trieChaine( char * chaine )
{
    int tailleChaine = strlen(chaine);

    for(int i=0; i< tailleChaine;i++)
    {
        for(int j=i+1; j < tailleChaine; j++)
        {
            if( *(chaine+i) > *(chaine+j) )
            {
                char tmp    = *(chaine+i);
                *(chaine+i) = *(chaine+j);
                *(chaine+j) = tmp;
            }
        }
    }
    printf("%s", chaine);
    return chaine;
}

int main( int argc, char * argv[])
{
    char* chaine = *(argv+1);
    trieChaine(chaine);

}
