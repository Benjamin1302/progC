#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct etudiant{
    char* nom;
    char* prenom;
    int notes[10];
};

struct etudiant initValue(char* nom, char* prenom)
{
    struct etudiant student;
    student.nom    = nom;
    student.prenom = prenom;
    for( int i=0; i< 10; i++)
    {
        int note = rand()%20;
        student.notes[i] = note;
    }

    return student;
}

int moyenneGeneral( struct etudiant student )
{
    int somme = 0;
    for(int i=0 ; i< 10; i++)
    {
        somme += student.notes[i];
    }

    return somme/10;
}

int moyenneClasse(int nbEleve)
{
    int sommeMoyenne= 0;
    for( int i=0; i< nbEleve ; i++)
    {
        struct etudiant student = initValue("benj","delb");
        sommeMoyenne += moyenneGeneral(student);
    }

    return sommeMoyenne / nbEleve ;

}


int main( int argc, char * argv[])
{
    srand(time(NULL));
    struct etudiant student1 = initValue("benjamin","delbreuve");

    float moyenne = moyenneGeneral(student1);
    printf("moyenne general de %s = %f \n", student1.nom , moyenne);

    int moyenneC = moyenneClasse(30);
    printf("moyenne classe = %d \n" , moyenneC);
}
