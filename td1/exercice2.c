#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
typedef struct Date Date;
struct date{
    int jours;
    int mois;
    int annee;
};

int isBissextiles(int annee)
{
    if( (annee %4 == 0 && annee/100 !=0) || annee%400==0)
    {
        return 1;
    }
    return 0;
}

int intervalJoursDates(Date date1, Date date2){
    int jours = 0;

    for(int annee=date1.annee;annee<date2.annee;annee++){
        if(isBissextile(annee)){
            jours+=366;
        }
        else{
            jours+=365;
        }
    }

    for(int mois= date1.mois;date1.mois<date2.mois;mois++){
        if(mois%2==0){
            jours+=30;
        }
        else{
            jours+=31;
        }
    }

    jours+=(date1.mois-date2.mois)*30;
    jours+=(date1.jour-date2.jour);
    return jours;
}


int main()
{
    Date date1;
    date1.jour=5;
    date1.mois=10;
    date1.annee=2014;

    Date date2;
    date2.jour=5;
    date2.mois=10;
    date2.annee=2018;

    int interval = intervalJoursDates(date1,date2);
    printf("Interval de jours = %d",interval);

    return 0;

}
