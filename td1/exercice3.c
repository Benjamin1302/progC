#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
typedef struct complexe complexe;
struct complexe{
    int reel;
    int imaginaire;
};


complexe addition(complexe nbr1, complexe nbr2)
{
    complexe nbr;
    nbr.reel = nbr1.reel + nbr2.reel;
    nbr.imaginaire = nbr1.imaginaire + nbr2.imaginaire ;

    return nbr;
}

complexe soustraction(complexe nbr1, complexe nbr2)
{
    complexe nbr;
    nbr.reel = nbr1.reel - nbr2.reel;
    nbr.imaginaire = nbr1.imaginaire - nbr2.imaginaire;
    return nbr;
}
