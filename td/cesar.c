#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* cesar( char* message , int* cle )
{
    char alphabet[26] = "abcdefghijklmnopqrstuvwxyz";

    char newChar;

    short cptKey = 0;

    short lenMessage = strlen(message);

    short lenKey = sizeof(cle);

    for( int i = 0 ; i < lenMessage ; i++ )
    {
        if( cptKey > lenKey )
        {
            cptKey = 0 ;
        }
        short key = cle[cptKey];

        if(key > 26 )
        {
            do
            {
                key -= 26;
            }
            while( key > 26 );
        }
        
        printf("[debug] key : %d \n", key);

        newChar = message[i]+key;
        if(message[i]+key > 'z')
        {
            int diff = 'z'-message[i];
            newChar= alphabet[diff];
            printf(" char de base : %c \n", message[i]+key);
            printf(" char new : %c \n", newChar);
        }

        printf("[debug] char message : %c \n", message[i]);
        printf("[debug] new char : %c \n", newChar );

        message[i] = newChar ;
        cptKey++;
    }

    return message;
}

int main( int argc, char* argv[])
{
    char* message = argv[1];
    printf("tableau : %s \n\n",argv[2]);
    int key[3];

    for(int i=0; i < argc-2 ; i++)
    {
        key[i] = atoi(argv[i+2]);
    }
    printf("valeur de la clé : %d \n", key[1] );

    message = cesar(message, key);
    printf("%s", message);
    return 0;
}


